import { useEffect, useState } from 'react';
import './App.css';
import axios from 'axios'


function App() {
  const [products, setProducts] = useState([]);

  
  useEffect(() => {
    console.log(products)
  }, [products])

  useEffect(() => {
    axios.get("https://dummyjson.com/products?limit=10")
      .then(data => {
        setProducts(data.data.products);
      })
  }, []);

  return (
    <div className="App">
      <h2>Products</h2>
      <div className="prod-list">
        {products.map(prod => (
          <div key={prod.id} className="prod-card">
            <img className="image" src={prod.images[0]} />
            <h3 className="title">{prod.title}</h3>
            <p className="brand">{prod.brand}</p>
            <p className="desc">{prod.description}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
